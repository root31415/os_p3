#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <limits>
#include <chrono> 
#include <pthread.h>
#include <iomanip>
using namespace std;
using namespace std::chrono; 

#define NUM_OF_THREADS 4
const int MAX_NUMBER = 1e6;

struct thread_data
{
   int thread_id;
   string message;
};

vector<vector<string>> weights_set;
vector<vector<string>> train_set;
vector<vector<string>> train_set_par[NUM_OF_THREADS];
vector<vector<string>> train_set_norm;
vector<vector<string>> train_set_norm_par[NUM_OF_THREADS];
float tmax[MAX_NUMBER], tmin[MAX_NUMBER];
float tmax_par[NUM_OF_THREADS][MAX_NUMBER], tmin_par[NUM_OF_THREADS][MAX_NUMBER];
int price_range_predicted[MAX_NUMBER];
int price_range_predicted_par[NUM_OF_THREADS][MAX_NUMBER];


vector<vector<string>> read_csv(string csv_addr){
	vector<vector<string>> set;
	ifstream file;
	file.open(csv_addr, ios::in);
	
	if(!file.is_open())
		throw std::runtime_error("Could not open file");
	
	string line, word;
	
    vector<string> row;
	int r=0;
	
	while (getline (file, line)) {
 		stringstream s(line);
        while (getline(s, word, ',')) {
            row.push_back(word);
        }
        set.insert(set.begin()+r, row);
        row.clear();
        r++;
	}
	return set;
}

void find_min_max_par(long tid){
	for(int j = 0; j < train_set_par[tid][0].size() - 1; j++){
		tmin_par[tid][j] = stof(train_set_par[tid][1][j]);
		tmax_par[tid][j] = stof(train_set_par[tid][1][j]);	
		for(int i = 2; i < train_set_par[tid].size(); i++){
			if(stof(train_set_par[tid][i][j]) > tmax_par[tid][j])
				tmax_par[tid][j] = stof(train_set_par[tid][i][j]);
			if(stof(train_set_par[tid][i][j]) < tmin_par[tid][j])
				tmin_par[tid][j] = stof(train_set_par[tid][i][j]);
		}
	}
	pthread_exit(NULL);
}

void* read_csv_parallel(void* args){
	struct thread_data* data = (struct thread_data*) args;
	int tid = data -> thread_id;
	string folder = data -> message;
	ifstream file;
	string csv_addr = folder + "train" + "_" + to_string(tid) + ".csv";
	file.open(csv_addr, ios::in);
	
	if(!file.is_open())
		throw std::runtime_error("Could not open file");
	
	string line, word;
	
    vector<string> row;
	int r=0;
	
	while (getline (file, line)) {
 		stringstream s(line);
        while (getline(s, word, ',')) {
            row.push_back(word);
        }
        train_set_par[tid].insert(train_set_par[tid].begin()+r, row);
        row.clear();
        r++;
	}
	train_set_norm_par[tid] = train_set_par[tid];
	find_min_max_par(tid);
	return NULL;
}


void find_accuracy_par(long tid){
	long temp = 0;
	for(int i = 1 ; i < train_set_norm_par[tid].size(); i++){
		int price_index = train_set_par[tid][0].size()-1;
		if(stoi( train_set_par[tid][i][price_index] ) == price_range_predicted_par[tid][i]){
			temp++;
		}
	}
	pthread_exit((void*)temp);
}

void classification_par(long tid){
	for(int i = 1; i < train_set_norm_par[tid].size(); i++){
		int index = 0;

		float score = 0;
		for(int j = 0; j < train_set_norm_par[tid][0].size() - 1; j++){
			score += ( stof(train_set_norm_par[tid][i][j]) * stof(weights_set[1][j]) );
		}
		score += stof( weights_set[1][weights_set[0].size() - 1] );
		float max_score = score;

		for(int k = 2; k < weights_set.size(); k++){
			float score = 0;
			for(int j = 0; j < train_set_norm_par[tid][0].size() - 1; j++){
				score += stof(train_set_norm_par[tid][i][j]) * stof(weights_set[k][j]);
			}
			score += stof( weights_set[k][weights_set[0].size() - 1] );
			if(score >= max_score){
				index = k - 1;
				max_score = score;
			}
		}
		price_range_predicted_par[tid][i] = index;
	}
	find_accuracy_par(tid);
}

void* normalization_par(void* args){
	long tid = (long)args;
	for(int i = 0; i < train_set_par[tid][0].size(); i++){
		train_set_norm_par[tid][0][i] = train_set_par[tid][0][i];
	}

	for(int j = 0; j < (train_set_par[tid][0].size() - 1); j++){
		for(int i = 1; i < train_set_par[tid].size(); i++){
			train_set_norm_par[tid][i][j] = to_string( (stof(train_set_par[tid][i][j]) - tmin[j]) / (tmax[j] - tmin[j]) );
		}
	}
	classification_par(tid);
	return NULL;
}



int main(int argc, char const *argv[])
{
	auto t0 = high_resolution_clock::now();

	thread_data th_data[NUM_OF_THREADS];

	void* status;
	string folder = argv[1];
	string weights_set_addr = folder + "weights.csv";
	
	pthread_t threads[NUM_OF_THREADS];

	for(long i = 0; i < NUM_OF_THREADS; i++){
		th_data[i].thread_id = i;
		th_data[i].message = folder;
		
		pthread_create(&threads[i], NULL, read_csv_parallel, (void*)&th_data[i]);
	}

	weights_set = read_csv(weights_set_addr);

	for(long i = 0; i < NUM_OF_THREADS; i++){
		pthread_join(threads[i], &status);
	}

	for(int j = 0; j < train_set_par[0][0].size(); j++){
		tmin[j] = tmin_par[0][j];
		tmax[j] = tmax_par[0][j];
		for(int i = 1; i < NUM_OF_THREADS; i++){
			if(tmin_par[i][j] < tmin[j])
				tmin[j] = tmin_par[i][j];
			if(tmax_par[i][j] > tmax[j])
				tmax[j] = tmax_par[i][j];
		}
	}

	for(long i = 0; i < NUM_OF_THREADS; i++){
		pthread_create(&threads[i], NULL, normalization_par, (void*)i);
	}

	long num_of_acc = 0;

	for(long i = 0; i < NUM_OF_THREADS; i++){
		pthread_join(threads[i], &status);
		num_of_acc += (long)status; 
	}

	long data_size = train_set_norm_par[0].size() + train_set_norm_par[1].size() + train_set_norm_par[2].size() + train_set_norm_par[3].size() - NUM_OF_THREADS;
	float acc = 100 * (float)num_of_acc / (float)data_size;
	cout << fixed;
	cout << "Accuracy: " << setprecision(2) << acc << "%" << "\n";
	auto t1 = high_resolution_clock::now();

	auto main_duration = duration_cast<microseconds>(t1-t0); 
	//cout << "main duration : " << main_duration.count() / 1000000.0 << endl;
 	
	return 0;
}