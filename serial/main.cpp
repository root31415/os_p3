#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <limits>
#include <chrono>
#include <iomanip>
using namespace std;
using namespace std::chrono;

const int MAX_NUMBER = 1e6;
 

vector<vector<string>> weights_set;
vector<vector<string>> train_set;
vector<vector<string>> train_set_norm;
float tmax[MAX_NUMBER], tmin[MAX_NUMBER];
int price_range_predicted[MAX_NUMBER];

vector<vector<string>> read_csv(string csv_addr){
	
	vector<vector<string>> set;
	ifstream file;
	file.open(csv_addr, ios::in);
	
	if(!file.is_open())
		throw std::runtime_error("Could not open file");
	
	string line, word;
	
    vector<string> row;
	int r=0;
	
	while (getline (file, line)) {
 		stringstream s(line);
        while (getline(s, word, ',')) {
            row.push_back(word);
        }
        set.insert(set.begin()+r, row);
        row.clear();
        r++;
	}
	return set;
}

void find_min_max(){
	for(int j = 0; j < train_set[0].size() - 1; j++){
		tmin[j] = stof(train_set[1][j]);
		tmax[j] = stof(train_set[1][j]);
		for(int i = 2; i < train_set.size(); i++){
			if(stof(train_set[i][j]) > tmax[j])
				tmax[j] = stof(train_set[i][j]);
			if(stof(train_set[i][j]) < tmin[j])
				tmin[j] = stof(train_set[i][j]);
		}
	}
}

void normalization(){

	for(int i = 0; i < train_set[0].size(); i++){
		train_set_norm[0][i] = train_set[0][i];
	}

	for(int j = 0; j < (train_set[0].size() - 1); j++){
		for(int i = 1; i < train_set.size(); i++){
			train_set_norm[i][j] = to_string( (stof(train_set[i][j]) - tmin[j]) / (tmax[j] - tmin[j]) );
		}
	}
}

void classification(){
	for(int i = 1; i < train_set_norm.size(); i++){
		int index = 0;

		float score = 0;
		for(int j = 0; j < train_set_norm[0].size() - 1; j++){
			score += ( stof(train_set_norm[i][j]) * stof(weights_set[1][j]) );
		}
		score += stof( weights_set[1][weights_set[0].size() - 1] );
		float max_score = score;

		for(int k = 2; k < weights_set.size(); k++){
			float score = 0;
			for(int j = 0; j < train_set_norm[0].size() - 1; j++){
				score += stof(train_set_norm[i][j]) * stof(weights_set[k][j]);
			}
			score += stof( weights_set[k][weights_set[0].size() - 1] );
			if(score >= max_score){
				index = k - 1;
				max_score = score;
			}
		}
		price_range_predicted[i] = index;
	}
}

float find_accuracy(){
	float temp = 0;
	for(int i = 1 ; i < train_set_norm.size(); i++){
		int price_index = train_set[0].size()-1;
		if(stoi( train_set[i][price_index] ) == price_range_predicted[i]){
			temp++;
		}
	}
	return( temp / ((float)(train_set.size() - 1)) );
}

int main(int argc, char const *argv[])
{

	auto t0 = high_resolution_clock::now();

	string folder = argv[1];
	string train_set_addr = folder + "train.csv";
	string weights_set_addr = folder + "weights.csv";
	train_set = read_csv(train_set_addr);
	train_set_norm = train_set;
	weights_set = read_csv(weights_set_addr);

	find_min_max();
	normalization();

	classification();

	float acc = find_accuracy();
	acc = acc * 100;
	
	cout << fixed;

	cout << "Accuracy: " << setprecision(2) << acc << "%" << "\n";

	auto t1 = high_resolution_clock::now();

	auto main_duration = duration_cast<microseconds>(t1-t0);

	//cout << "main duration : " << main_duration.count() / 1000000.0 << endl;

	return 0;
}